﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Financeiro.Cobranca.Api.Debitos.Tarefas
{
    /// <summary>
    /// A business service with a functional boundary directly associated 
    /// with a specific parent business task or process is based on the task
    /// service model. This type of service tends to have less reuse potential
    /// and is generally positioned as the controller of a composition responsible
    /// for composing other, more process-agnostic services.
    /// </summary>
    /// 
    /// <example>
    /// If, however, we had a billing consolidation process that retrieved numerous
    /// invoice and PO records, performed various calculations, and further validated
    /// consolidation results against client history billing records, we would have process 
    /// logic that spans multiple entity domains and does not fit cleanly within a functional ,
    /// context associated with a business entity. This would typically constitute a "parent" 
    /// process in that it consists of processing logic that needs to coordinate the involvement
    /// of multiple services 
    /// </example>

    public class ParcelamentoDebitoController
    {
    }
}
