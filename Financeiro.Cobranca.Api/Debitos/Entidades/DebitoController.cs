﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Financeiro.Cobranca.Api.Debitos.Entidades
{
    /// <summary>
    /// In just about every enterprise, there will be business model documents that 
    /// define the organization's relevant business entities. Examples of business 
    /// entities include customer, employee, invoice, and claim.
    /// </summary>
    /// 
    /// <example>
    /// Several of its capabilities are reminiscent of traditional CRUD (create, read, update, delete) methods.
    /// </example>
    public class DebitoController
    {
    }
}
