﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Financeiro.Cobranca.Api.Debitos.Utilitarios
{
    /// <summary>
    /// The utility service model accomplishes this. It is dedicated to providing reusable, 
    /// cross-cutting utility functionality, such as event logging, notification, and exception
    /// handling. It is ideally application agnostic in that it can consist of a series of 
    /// capabilities that draw from multiple enterprise systems and resources, while making 
    /// this functionality available within a very specific processing context.
    /// </summary>
    /// 
    /// <example>
    /// The example on the right shows a utility service providing a set of capabilities associated with proprietary data format transformation.
    /// </example>
    public class CalculoDatasController
    {

    }
}
